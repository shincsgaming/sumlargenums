﻿namespace HelloWorld
{
    class MyBigNumber
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Số thứ 1: ");
            //string num1 = Console.ReadLine();
            //Console.WriteLine("Số thứ 2: ");
            //string num2 = Console.ReadLine();
            string num1 = "99999";
            string num2 = "9999999999";
            var result = Sum(num1,num2);
            Console.WriteLine("Result: " + result);
        }
        static string Sum(string num1, string num2)
        {
            int num1L = num1.Length;
            int num2L = num2.Length;

            if (num1L > num2L)
            {
                string temp = num1;
                num1 = num2;
                num2 = temp;
            }

            string ResultStr = "";
            char[] num1ch = num1.ToCharArray();
            char[] num2ch = num2.ToCharArray();
            Array.Reverse(num1ch);
            Array.Reverse(num2ch);
            num1 = new string(num1ch);
            num2 = new string(num2ch);

            int plus = 0;
            int result = 0;
            int step = 1;

            for (int i = 0; i < num1L; i++)
            {
                result = (int)(num1[i] - '0') + (int)(num2[i] - '0') + plus;
                ResultStr += (char)(result % 10 + '0');
                plus = result / 10;
                Console.WriteLine(string.Format("Bước {0}  : {1} + {2} = {3} nhớ {4} ", step, num1[i], num2[i], result, plus));
                step++;
            }
            for (int i = num1L; i < num2L; i++)
            {
                result = (int)(num2[i] - '0') + plus;
                ResultStr += (char)(result % 10 + '0');
                plus = result / 10;
            }
            if (plus > 0)
            {
                ResultStr += (char)(plus + '0');
            }
            char[] resultch = ResultStr.ToCharArray();
            Array.Reverse(resultch);
            ResultStr = new string(resultch);

            return ResultStr;
        }
    }
    
}